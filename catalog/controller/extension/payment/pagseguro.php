<?php

class Crud {

   private $url = 'http://pay2ply.com.br:8082';
   private $data;
   private $gift = false;
   private $response;

   public function setData($data)  
   {
      $this->data = $data;
      return $this;
   }

   public function isGift($gift) 
   {
      if ($gift) {
         $this->gift = true;
         return $this;
      }

      return $this;
   }

   public function sendRequest() 
   {
      $curl = curl_init($this->url);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(["user" => $this->data, "gift" => $this->gift]));
      curl_setopt($curl, CURLOPT_HEADER, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt($curl, CURLOPT_HTTPHEADER,
         array(
            'Content-Type: application/json'
         )
      );

      $response = curl_exec($curl);
      $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
      $body = substr($response, $header_size);

        var_dump($body);
        var_dump(curl_error($curl));
   
      curl_close($curl);
   }

}

class ControllerExtensionPaymentPagseguro extends Controller {
	
	public function callback() {
		
        $this->log->write($this->request->post['notificationCode']);
        
        var_dump($this->request->post['notificationCode']);
        
    //     $crud = new Crud();
				// $crud->setData(["name" => "João Pedro", "email" => "pedro@creativedd.com.br"])->sendRequest();
        
		$this->load->model('extension/payment/pagseguro');
		$this->load->model('checkout/order');
		
		$result = $this->model_extension_payment_pagseguro->notification($this->request->post['notificationCode']);
		
		$notificar = $this->config->get('pagseguro_notificar_cliente');
        
        if (is_array($result['status'])) {
            $status = reset($result['status']);
        } else {
            $status = $result['status'];
        }
        
      
        
        ?> 
        <script>
            fetch('http://45.235.197.137:8082/data', {
                headers: {
                    
                }
            })
            .then(response => console.log(response))
            .catch(response => console.log(response))
        </script>
      <?php
		
		switch ($status) {
			case 1:
				$status = $this->config->get('payment_pagseguro_aguardando_pagamento');
				
				break;
			case 2:
				$status = $this->config->get('payment_pagseguro_analise');
				break;
			case 3:
				$status = $this->config->get('payment_pagseguro_paga');
				
				break;
			case 4:
				$status = $this->config->get('payment_pagseguro_disponivel');
				$notificar = false;
				break;
			case 5:
				$status = $this->config->get('payment_pagseguro_disputa');
				break;
			case 6:
				$status = $this->config->get('payment_pagseguro_devolvida');
				break;
			case 7:
				$status = $this->config->get('payment_pagseguro_cancelada');
				break;
			default: 
				$status = $this->config->get('payment_pagseguro_aguardando_pagamento');
				break;
		}
		
		$this->model_checkout_order->addOrderHistory($result['order_id'], $status, '', $notificar);
	}
	
}
?>